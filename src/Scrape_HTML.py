#!/usr/bin/python
#################################################################################
# FILENAME   : Scrape_HTML.py                                                   #
# AUTHOR     : Brian Denton <brian.denton@gmail.com>                            #
# DATE       : 11/20/2012                                                       #
# DESCRIPTION: Scrape data from an HTML table on a webpage and pass the data to #
#              R to demonstrate RPy2.                                           #
#################################################################################

from urllib import urlopen
from BeautifulSoup import BeautifulSoup
import re
import rpy2.robjects as robjects

# Copy all content from the provided web page
URL="http://apps.nccd.cdc.gov/BRFSS/list.asp?cat=TU&yr=2010&qkey=4396&state=All"
CDC = urlopen( URL ).read()

# Use BeautifulSoup to parse webpage elements using HTML tags
soup = BeautifulSoup( CDC )

# Get all tables in the webpage
Tables = soup.findAll('table')

# Find the table with the appropriate title

TABLE_TITLE = "Adults who are current smokers"

# Scan tables and find the index of the table with the title we want 
TABLE_INDEX = [i for i in range(len(Tables)) if TABLE_TITLE in str(Tables[i])][0]

SmokingTable = Tables[TABLE_INDEX]

# Make a Python list out of the table's rows (tr is the HTML tag for table row)
SmokingTable = SmokingTable.findAll('tr')

# Select the rows containing the desired data
SmokingTable = SmokingTable[-54:]

# Parse each row and populate lists for State and Smoking
State = []
Smoking = []
MapColor = []

for row in SmokingTable:
    row_list = re.split( '<|>', str(row) )
    state = row_list[6]
    smoking = float(row_list[12])
    State.append( state )
    Smoking.append( smoking )
    if( smoking <= 15 ):
        MapColor.append( "pink" )
    elif( smoking > 15 and smoking <= 20 ):
        MapColor.append( "red" )
    elif( smoking > 20 ):
        MapColor.append( "dark red" )
    else:
        MapColor.append( "" )


# Write a Python wrapper function that converts Python data types to R data types
# using RPy2 and calls R function

def map_plot_py( regions, color, title,
                 legend_placement, legend_text, legend_color, outfile ):

    # Convert Python lists to R string vectors
    regions_R = robjects.StrVector( regions )
    color_R =  robjects.StrVector( color )
    legend_text_R = robjects.StrVector( legend_text )
    legend_color_R =  robjects.StrVector( legend_color )

    # Load R source and bind the map_plot function to the name map_plot_R in the
    # Python global environment.
    source_R = robjects.r( 'source("../lib/map_plot.R")' )
    map_plot_R = robjects.globalenv['map_plot']

    # Pass R data vectors to R function
    map_plot_R( regions = regions_R, color = color_R,
          title = title,
          legend_placement = legend_placement,
          legend_text = legend_text_R,
          legend_color = legend_color_R,
          outfile = outfile )

    return None


# map_plot_py takes Python data objects as inputs
map_plot_py( regions = State, color = MapColor,
             title = "Smoking Prevalence in 2010",
             legend_placement = "bottomleft",
             legend_text = ["<15%","15%-20%",">20%"],
             legend_color = ["pink","red","dark red"],
             outfile = "../plots/Smoking_Prevalence_in_2010.pdf" )



## END OF FILE
