#!usr/bin/dev/python
#################################################################################
# FILENAME   : hw.py
# AUTHOR     : Brian Denton <brian.denton@gmail.com>
# DATE       : 02/11/2012
# DESCRIPTION: Python script to interface with C functions
#################################################################################

from ctypes import *

hw_lib = CDLL( "./hw.so" )

##############################################
# C function with non-void return type       #
##############################################

##  Specify the C data type of the return value for hw1().
hw_lib.hw1.restype = c_double  # restype

## Specify the C data types of the arguments passed to hw1().
s1 = hw_lib.hw1( c_double(1), c_double(2.141592653589793) )

# s1 should now have a value very close to zero as hw3() computes
# sin( 1 + 2.141592653589793 ) ~ sin( pi ) ~ 0 (where ~ denotes 
# approximately equal).
print s1, type(s1)

##############################################
# C function with void return type           #
##############################################

## If the C function return type is void you can declare a restype of None in
## Python, though this is optional.
hw_lib.hw2.restype = None

## If the argument data types are pre-specified it is not necessary to specify
## them when the function is called.
## If a function takes multiple arguments specify the data types in a list.

hw_lib.hw2.argtypes = [ c_double, c_double ] #pre-specified argtypes

hw_lib.hw2( 1, 2.141592653589793 )  #no return value, prints string to console


###############################################
# Passing data to C function by reference     #
###############################################

# Return type is void so no need to specify restype

# Pre-specify argument data types. The third argument is pass-by-reference.
# In this example, we create a variable s3 of type double and pass it by 
# reference to the function hw3().
s3 = c_double()
hw_lib.hw3.argtypes = [ c_double, c_double, POINTER(c_double) ]

# When calling the function we must indicate any arguments that are 
# pass-by-reference with the wrapping function pointer().
hw_lib.hw3( 1, 2.141592653589793, pointer(s3) )

print s3, type(s3)

###############################################################
# Providing an alias in the Python namespace to C++ objects   #
###############################################################

# The C++ function can apparently only be accessed through the dynamically loaded
# library hw_lib -- i.e. via the dot operator. But for brevity of syntax you can 
# define an alias in the Python namespace.


# Assign the C++ function in the DLL to a simpler Python name
hw3 = hw_lib.hw3

s4 = c_double()   

from math import pi

hw3( 0, pi/2, pointer(s4) ) # sin( 0 + pi/2 ) = 1

print s4, type(s4)

## END OF FILE
