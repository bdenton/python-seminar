#!/usr/bin/python
#################################################################################
# FILENAME   : Pandas.py                                                        #
# AUTHOR     : Brian Denton <brian.denton@gmail.com>                            #
# DATE       : 10/04/2012                                                       #
# DESCRIPTION: Demonstrate the functionality of the Pandas module.              #
#              http://pandas.pydata.org/                                        #
#              http://pandas.pydata.org/pandas-docs/stable/pandas.pdf           #
#                                                                               #
#              Most of the code in the following examples comes directly from   #
#              the pandas user guide linked above.                              #
#################################################################################

from pandas import *
from numpy.random import randn
import numpy as np

#################################################################################
# 1. Data Structures                                                            #
#################################################################################

##########
# Series #
##########

# Series is a one-dimensional labeled array (technically a subclass of ndarray) 
# capable of holding any data type (integers, strings, floating point numbers, 
# Python objects, etc.). The axis labels are collectively referred to as the
# index.

# Create a Series that contains five random numbers indexed with the letters
# a through e.

s = Series(randn(5), index=[ 'a', 'b', 'c', 'd', 'e' ])
s
s.index

# The default indexing for Series is unique integer values starting at 0.

Series(randn(5))

# If you have previously created a dict you can created a Series from that dict.

d = { 'a':0., 'b':1., 'c':2. }

# If no index is specified for the Series the indices will be constructed from
# the sorted keys of the dict.
Series( d )

# If an index is specified the Series will look up the values in the dict
# according to the key values. Any index value not found in the dict will be
# assigned a missing value in the Series.
Series( d, index=['b', 'c', 'd', 'a'] )

# Elements in Series objects can be accessed much like numpy.ndarray objects.

s[0]               # Return first element
s[:3]              # Return first three elements (indices 0 to 2)
s[s > s.median()]  # Return all elements greater than the median value
s[[4, 3, 1]]       # Return the 5th, 4th, and 2nd elements
np.exp(s)          # Return the exponential of all elements in the Series

# Elements in Series objects can be accessed much like dict objects.

s['a']             # Return Series value with index 'a'
s['c'] = 10        # Set Series value with index 'c' to 10

'b' in s           # Test if Series has an index 'b'
'k' in s           # Test if Series has an index 'k'

# Mathematical operations on Series objects are vectorized such that the 
# operation is executed on each element of the Series. This similar to
# numpy.ndarray and data operatons in R.

s + s
2 * s
np.exp(s)

# Series indices are automatically aligned so.

s_drop_first = s[1:]
s_drop_last  = s[:-1]

s_drop_first
s_drop_last

s_drop_first + s_drop_last # Any index not present in both Series will be NaN

# As demonstrated above, the default behavior for Series operations with 
# non-aligned indices is to return the union of the indices with non-aligned
# indices having value NaN. If you prefer the result to contain only indices
# present in both Series use the dropna() method.

( s_drop_first + s_drop_last ).dropna()

#############
# DataFrame #
#############

# DataFrame is a 2-dimensional labeled data structure with columns of 
# potentially different types. You can think of DataFrame as a dict of Series
# objects -- the values of the dict are Series objects containing columns of 
# data and the keys of the dict are the column names. 
# DataFrame is modeled closely after the R data.frame.

# Create a DataFrame from a dict of Series objects
d = { 'one':Series( [1., 2., 3.], index = ['a', 'b', 'c'] ),
      'two': Series( [1., 2., 3., 4.], index = ['a', 'b', 'c', 'd'] )}

df = DataFrame( d )

# Create a DataFrame using only the specified indices
DataFrame( d, index = ['d', 'b', 'a'] )

# Create a DataFrame using only the specified indices and columns
DataFrame( d, index = ['d', 'b', 'a'], columns = ['two', 'three'] )

# Return DataFrame index and columns
df.index
df.columns

# Missing values in a DataFrame are represented using np.nan
df['two'][0] = np.nan

# New columns can be created from functions of existing columns
df['three'] = df['one'] * df['two']
df['flag'] = df['one'] > 2

# Columns can be removed from a DataFrame using del
del df['two']

# Columns can be extracted from a DataFrame using pop
three = df.pop('three')

# When inserting a scalar into a DataFrame the value will be recycled as 
# needed to fill the column

df['foo'] = 'bar'




# Select a DataFrame column
df['one']

# Select row by label
df.xs('a')
df.ix['a']

# Select row by location (int)
df.ix[0]

# Slice rows
df[0:3]

# Select subset
df.ix['a','flag']

# Select rows by boolean vector
df[ df['one'] <= 2 ]


# As with Series objects operations on DataFrames automatically align the 
# indices.
X = DataFrame(randn(10, 4), columns=['A', 'B', 'C', 'D'])
Y = DataFrame(randn(7, 3), columns=['A', 'B', 'C'])

XY = X + Y

# Can specify a fill value to replace NaN
XY0 = X.add( Y, fill_value = 0 )

# Matrix operations

# The .transpose method or the .T method transpose a DataFrame
X.transpose()
X.T

# The .dot method performs matrix multiplication

XtX = (X.T).dot( X )



# Create DataFrame from reading in text file

PK = read_csv( '../data/Indometh.csv' )

# Execute some simple functions on DataFrame objects
PK.shape
PK.head()
PK.tail()

# Find median concentration per patient
PK_grouped = PK.groupby('Subject')
PK_grouped['conc'].apply( np.median )

# Find the time when concentration is half its max value

# Step 1: Find half of each subject's max conc values
C_half = PK_grouped.max()['conc']/2

# Step 2: Convert to a dictionary for easy lookup
C_half_dict = dict( C_half )

# Step 3: Vectorize the dictionary so we can pass a vector of subject ID's
#         and get C_half for each one
C2 = np.vectorize(lambda x: C_half_dict[x] )

# Step 4: Create a Series from the vector of concentrations created in Step 3
C2_Series = Series( C2( PK[['Subject']] ).flatten().tolist() )

# Step 5: Retain rows in original PK DataFrame that have a conc <= C_half
PK_C_half = PK[ PK['conc'] <= C2_Series ]

# Step 6: Group remaining rows by Subject
PK_C_half = PK_C_half.groupby('Subject')

# Step 7: For each Subject retain the first time point where conc <- C_half.
PK_C_half = PK_C_half.first()


# Create a pivot table
PK_pivot = pivot_table( PK, values = 'conc', rows = ['time'], cols = ['Subject'] )

# Plot the pivot table
PK_pivot.plot()


#########
# Panel #
#########

# Used to represent panel data. Common in econometrics.
# Not covered here.

#################################################################################
# 3. Indexing and Selecting Data                                                #
#################################################################################

# Drop duplicate rows
dup = PK[0:5].append( PK[0:5] )
dup.drop_duplicates()



#################################################################################
# 4. Computational Tools                                                        #
#################################################################################

# Compute percent change within a Series
X['A']
X['A'].pct_change()
X['A'].pct_change( periods = 3 )


# Compute covariance between two Series
X['A'].cov( X['B'] )

# Compute covariance matrix
X.cov()

# Corrlations are computed similarly
X['A'].corr( X['B'] )
X.corr()

# Note that non-numeric columns will be automatically excluded from the 
# correlation calculation.

# Get rank of data values

Ex = Series( [1,2,5.5,3,-12,7,2,11.2,23,7] )
Ex
Ex.rank()
Ex.rank( ascending = False )

# Ranking rules for ties
Ex.rank( method = 'average' )
Ex.rank( method = 'min' )
Ex.rank( method = 'max' )
Ex.rank( method = 'first' )

# There are a number of rolling statistics

# rolling_count         Number of non-null observations
# rolling_sum           Sum of values
# rolling_mean          Mean of values
# rolling_median        Arithmetic median of values
# rolling_min           Minimum
# rolling_max           Maximum
# rolling_std           Unbiased standard deviation
# rolling_var           Unbiased variance
# rolling_skew          Unbiased skewness (3rd moment)
# rolling_kurt          Unbiased kurtosis (4th moment)
# rolling_quantile      Sample quantile (value at %)
# rolling_apply         Generic apply
# rolling_cov           Unbiased covariance (binary)
# rolling_corr          Correlation (binary)
# rolling_corr_pairwise Pairwise correlation of DataFrame columns


rolling_sum( Ex, window = 2 )

# The rolling methods can be particularly useful to fime series objects

ts = Series( randn(5*365), index = date_range('1/1/2000', periods = 5*365) )
ts = ts.cumsum()

# Plot 90-day moving average
ts.plot( style = 'k--' )
rolling_mean( ts, window = 90).plot( style = 'r' )


# Regression

from pandas.io.data import DataReader
symbols = ['MSFT', 'GOOG', 'AAPL']

data = dict((sym, DataReader(sym, "yahoo"))
            for sym in symbols)

panel = Panel(data).swapaxes('items', 'minor')

close_px = panel['Close']

rets = close_px / close_px.shift(1) - 1

rets.info()

simple = ols( y = rets['AAPL'], x = rets.ix[:,['GOOG']] )

simple

simple.beta


multiple = ols( y = rets['AAPL'], x = rets.drop('AAPL', axis = 1) )




#################################################################################
# 5. Working with Missing Data                                                  #
#################################################################################

rowlabels = ['a','c','e','g']

D = DataFrame( {'one':Series( (1,1,1,1), index = rowlabels ),
                'two':Series( randn(4), index = rowlabels ), 
                'three':Series( ('red','blue','green','white'), index = rowlabels )})

# Reindexing often introduces missing values
D = D.reindex_axis( ['a','b','c','d','e','f','g','h'], axis = 0 )
        
# Missing values are omitted from calculations, but may be retained in a result
# array unless explicitly removed.

D['one']
sum( D['one'] )
D['one'].cumsum()
D['one'].cumsum().dropna()

# Missing values can be filled with a scalar.

D['two'].fillna(0)
D['three'].fillna('missing')

# Missing values can also be populated by carrying forward a previous  
# non-missing value or carrying backward a subsequent non-missing value.

D['two'].fillna( method = 'ffill' ) # Forward fill
D['two'].fillna( method = 'bfill' ) # Backward fill

# You can limit the number of missing values that will be populated by a 
# non-missing value carried forward or backward.

D['three']['e':] = np.nan # First, make several consecutive missing values.
D['three']

D['three'].fillna( method = 'ffill' ) # Forward fill all missing values
D['three'].fillna( method = 'ffill', limit = 2 ) # Forward fill at most two values


#################################################################################
# 6. Group By: split-apply-combine                                              #
#################################################################################

# Extract values from groups or apply a function over groups

s = Series( [10, 100, 20, 200, 2000, 30], index = [1, 1, 2, 2, 2, 3] )

grouped = s.groupby( level = 0 )

grouped.first()

grouped.last()

grouped.sum()



df = DataFrame({'A' : ['foo','bar','foo','bar','foo','bar','foo','foo'],
                'B' : ['one','one','two','two','two','two','one','three'],
                'C' : randn(8),
                'D' : randn(8)})


# Aggregate and apply function to groups
grouped = df.groupby('A')

grouped.aggregate(np.sum)

grouped = df.groupby(['A','B'])

grouped.aggregate(np.sum)

# Get size of groups
grouped.size()


# Apply multiple functions simulataneously
grouped['C'].agg([np.sum, np.mean, np.std])

# I would change NaN for std (where size == 1) to 0
grouped['C'].agg([np.sum, np.mean, np.std]).fillna(0)

# Return groups in sorted order or non-sorted order

df2 = DataFrame({'X' : ['B','B', 'A', 'A'], 'Y' : [1, 2, 3, 4]})

df2.groupby(['X'], sort = True ).sum()

df2.groupby(['X'], sort = False ).sum()


#################################################################################
# 7. Merge, Join, and Concatenate                                               #
#################################################################################

# Create DataFrame of numbers with non-aligned indices
df1 = DataFrame( randn(5,4), index = [range(5)] )
df2 = DataFrame( randn(5,4), index = [i + 10 for i in range(5)] )
df3 = DataFrame( randn(5,4), index = [range(5)] )

# Concatenate DataFrame objects
concat([df1,df2])
df1.append(df2)

# You can label the source DataFrames in the concatenated form
concat( [df1,df2], keys = ['df1','df2'] )

# Concatenating DataFrames that have overlapping row indices will cause the index
# values to be non-unique. You can create a new index with ignore_index = True.
concat( [df1,df3], ignore_index = True )

# Rename the DataFrame columns to facilitate some join operations

df1.columns = ['A','B','C','D']
df2.columns = ['A','X','Y','Z']
df3.columns = ['A','B','C','D']

df2['A'][:] = df1['A'][:] # Need [:] or result is all NaN b/c no index overlap
df2['X'][:] = df1['A'][:]

df2['Y'][0:3] = df1['A'][0:3]

# merge(left, right, how=left, on=None, left_on=None, right_on=None,
# left_index=False, right_index=False, sort=True,
# suffixes=(.x, .y), copy=True)

# Merge two DataFrames on their single common column.
# As in R, when the merge key is unambigous -- i.e. both DataFrames have a
# single common column -- the 'on' parameter is optional.
merge( left = df1, right = df2, on = 'A' )

# When the merge key is different in the two DataFrames the merge keys must be
# provided.
merge( left = df1, right = df2, left_on = 'A', right_on = 'X' )

merge( left = df1, right = df2, how = 'inner', left_on = 'A', right_on = 'Y' )
merge( left = df1, right = df2, how = 'left',  left_on = 'A', right_on = 'Y', left_index = True )
merge( left = df1, right = df2, how = 'right', left_on = 'A', right_on = 'Y', right_index = True )
merge( left = df1, right = df2, how = 'outer', left_on = 'A', right_on = 'Y', suffixes = ('_df1','_df2') )


## END OF FILE
