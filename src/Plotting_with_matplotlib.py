#!/usr/bin/python
#################################################################################
# FILENAME   : Plotting_with_matplotlib.py                                      #
# AUTHOR     : Brian Denton <brian.denton@gmail.com>                            #
# DATE       : 11/19/2012                                                       #
# DESCRIPTION: Demonstrate the Python plotting module matplotlib.               #
#              Examples taken from:                                             #
#                  https://www.youtube.com/watch?v=3Fp1zn5ao2M&feature=relmfu   #
#################################################################################

# The matplotlob gallery is a good place to browse the types of plots available
# with matplotlib and get the source code.
# http://matplotlib.org/gallery.html

from numpy import linspace, sin, cos, sqrt
from pylab import *
from math import pi
from numpy.random import rand

x = linspace( start = 0, stop = 2*pi, num = 50 )

# plot versus index
plot( sin(x) )

# plot versus actual x values
plot( x, sin(x) )

# multiple functions on same plot
plot( x, sin(x), x, sin(2*x) )

# just keep addding x,y pairs
plot( x, sin(x), x, sin(2*x), x, sin(3*x) )

#formatting strings: see plot? for avalailable options
plot( x, sin(x), 'r-o', x, sin(2*x), 'b--^', x, sin(3*x), 'g:s' )

# scatter plots
scatter( x, sin(x) )



# scatter function can figure out how to plot higer dimensions
x1 = rand(200)
y1 = rand(200)
size = rand(200)*30
color = rand(200)

scatter( x1, y1, size, color )
colorbar()


# print side-by-side plots
# subplot( ROWS, COLUMNS, ACTIVE_PLOT )
subplot( 2, 1, 1 )
plot( x, sin(x) )
plot( x, sin(2*x) )
plot( x, sqrt(x) )

subplot( 2, 1, 2 )
plot( x, cos(x) )

# Sharing axes between plots

plt1 = subplot( 2, 2, 1 )
plot( rand(50) )

plt2 = subplot( 2, 2, 2 )
plot( 2 * rand(100 ) )
# suppres x-axis tick labels
setp( plt2.get_xticklabels(), visible = False )

plt3 = subplot( 2, 2, 3, sharex=plt1 )#share x-axis with plt1
plot( rand(100) )

plt4 = subplot( 2, 2, 4, sharex = plt2, sharey = plt3 )
plot( 0.5 * rand(100) )
setp( plt4.get_yticklabels(), visible = False )


# Create a secondary y-axis

ax1 = subplot(111)  # A 1x1 matrix of plots -- i.e. all plots on same coordinate plane

plot( x, sin(x), 'b-')

ax2 = twinx()  # re-use x-axis

plot( x, 2*cos(5*x), 'r--')

show()



# Adding labels to a plot
# title, xlabel, ylabel, supports LaTeX
rc( 'text', usetex = True )
title('sine curves')
xlabel('radians', fontsize = 'large' )
ylabel('sin', fontsize = 'medium')
plot( x, sin(x), label = "$\sin(\psi)$" )
plot( x, sqrt(x/2), label = r"$\sin(\frac{\alpha}{2})$" )
legend()



## END OF FILE
