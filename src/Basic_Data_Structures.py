#!/usr/bin/python
#################################################################################
# FILENAME   : Basic_Data_Structures.py                                         #
# AUTHOR     : Brian Denton <brian.denton@gmail.com>                            #
# DATE       : 09/23/2012                                                       #
# DESCRIPTION: Demonstrate basic data structures and various other introductory #
#              concepts in Python programming.                                  #
#################################################################################

# Assign values to variables
int1 = 7
print int1, "\n", type(int1)

float1 = 7.0
print float1, "\n", type(float1)

# Unlike R, Python distinguishes between integer division and floating point division
int1/2
float1/2

str1 = "This is a string."
print str1, "\n", type(str1)

# Strings are immutable

#str1[0] = 'D'  # This generates an error

#################################################################################
# Lists are an ordered sequence of objects.                                     #
# Lists are homogenous (all elements are of the same type).                     #
# Lists are mutable.                                                            #
# Lists can also be nested - i.e. a list can contain another list.              #
#################################################################################

# Define a list
list1 = [0,1,2,3,4]

# Change a value (lists are mutable)
list1[4] = 10

# Nested lists within a list
list2 = [ ['A','B'], ['C','D'], ['E','F'] ]

# Note that you **can** create a heterogeneous list. The Python interpreter will
# not generate an error. But the Python convention is that lists are homogeneous.
# If you want to define a heterogeneous collection it is preferred that you
# use a tuple (described below). But if your collection must be mutable then you
# are forced to use a list even if it is heterogenous, as follows:

heterogenous_list = [10,'a', list1]  # Avoid if possible.

# Python index is zero-based and has pattern [first,last).

len( list1 )

list1[0:3]

# List comprehensions

numbers = [ i for i in range(10) ]

evens = [ i for i in range(20) if i % 2 == 0 ]


#################################################################################
# Tuples are an ordered sequence of objects.                                    #
# Tuples are heterogenous (elements can be of different types).                 #
# Tuples are immutable.                                                         #
# Tuples can also be nested - i.e. a tuple can contain another tuple.           #
# Tuples are used when the position of the elements matters. For example,       #
# an ordered pair of x-y coordinates. Tuples are often used as arguments        #
# passed to or returned from functions.                                         #
#################################################################################

# Define a tuple
tuple1 = (0,1,2,3,4)

# Attempting to change a tuple results in an error (tuples are immutable)
#tuple1[4] = 10  # Generates an error

# Nested tuples within a tuple
tuple2 = ( ('A','B'), ('C','D'), ('E','F') )


#################################################################################
# A dict (or dictionary) allows keyword lookups                                 #
#################################################################################
Person = { 'First':'Robert', 'Last':'Jones',' Gender':'Male', 'Age':57 }

Person['First']
Person['Age']

Person['Allergies'] = ['None']
print Person

#################################################################################
# Functions                                                                     #
#################################################################################

def is_even( x ):

    even_number = False

    if x % 2 == 0:
        even_number = True

    return even_number


is_even( 7 )


# A Python docstring can provide information about the function
def is_even( x ):
    """
    True if x is even, False otherwise.
    
    Args:
       x (numeric): the to test whether even
    Returns:
       True if x is even, False otherwise
    Raises:
       None
    """
    even_number = False

    if x % 2 == 0:
        even_number = True

    return even_number
    

is_even( 256 )

# Two (of the several) ways to learn more about a function.
print is_even.__doc__

from pydoc import help
help( is_even )

# The documentation examples provided above demonstrate some of Python's
# capabilities for introspection -- i.e. the ability to tell you about an
# object's attributes. There is also an inspect module that can extend
# Python's introspection capabilties.

from inspect import getsource

print getsource( is_even )

#################################################################################
# File I/O                                                                      #
#################################################################################

# Open a file, iterate through the lines of the file printing each one, and 
# close the file.

sample = open( "../data/sample.txt", "r" )

for line in sample:
    print line

sample.close()

# Make a copy of the file with line numbers

sample = open( "../data/sample.txt", "r" )
sample_with_line_numbers = open( "../data/sample_with_line_numbers.txt", "w" )

i = 1
for line in sample:
    sample_with_line_numbers.write( "[" + str(i) + "]\t" + line )
    i = i + 1

sample_with_line_numbers.close()
sample.close()



#################################################################################
# Objects                                                                       #
#################################################################################

# Object constructors are defined in the __init__() method.
# An explicit parameter self is always passed for class variables and methods.

class Vehicle(object):

    def __init__( self, color ):
        self.color = color

    def get_color( self ):
        return self.color


vehicle1 = Vehicle( "red" )
vehicle1.get_color()


# Inheritance is exploited with a simple call to the base class constructor.
# Python permits multiple inheritance (not demonstrated here).

# Inheritance syntax:   class Derived(Base):

class Car( Vehicle ):

    def __init__( self, color, doors ):
        Vehicle.__init__( self, color ) # call to base class constructor
        self.doors = doors

    def get_doors( self ):
        return self.doors


car1 = Car( "black", 2 )
car1.get_color()
car1.get_doors()
        

## END OF FILE
