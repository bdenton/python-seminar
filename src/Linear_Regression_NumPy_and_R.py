#!/usr/bin/python
#################################################################################
# FILENAME   : Linear_Regression_NumPy_and_R.py                                 #
# AUTHOR     : Brian Denton <brian.denton@gmail.com>                            #
# DATE       : 10/07/2012                                                       #
# DESCRIPTION: Demonstrate matrix computations with NumPy and an interface to R #
#################################################################################

from numpy.random import randn
from numpy import array, matrix
from numpy.linalg import inv

import sys
sys.path.append( "../lib" )
from helper_functions import add_intercept, matrix2csv


#################################################################################
# Generate some random data and write to csv files                              #
#################################################################################

p = 5
N = 10

Data = matrix( randn(N,p) )
y = matrix( randn(N,1) )

X = add_intercept( Data )

# Write data to files
matrix2csv( X, "../data/X.csv" )
matrix2csv( y, "../data/y.csv" )

#################################################################################
# Perform NumPy matrix algebra                                                  #
#################################################################################

XtX = X.T * X

beta = inv( XtX ) * (X.T * y)

print beta

#################################################################################
# Interface with R and compare NumPy results to glm() results                   #
#################################################################################

from rpy2.robjects.packages import importr
from rpy2.robjects.vectors import Matrix
from rpy2.robjects import globalenv
from rpy2.robjects.numpy2ri import activate

activate() # Activate translation of Python objects to R

# Import stats library
stats = importr("stats")

# Translate NumPy matrix objects to R matrix objects.
y_R = Matrix( array( y ) )
X_R = Matrix( array( Data ) ) # Use X data without column of ones.

# Copy R objects to global environment
globalenv["y"] = y_R
globalenv["X"] = X_R

# Estimate linear regression with glm()
glm_fit = stats.glm("y ~ X" )

print stats.summary_glm( glm_fit )


##############################
# %load_ext magic function   #
##############################

%load_ext rmagic
%R fit=glm('y~X')
%R R_beta = coef( fit )
%R print(R_beta)
%R print(summary(R_beta))
%R plot(fit)







## END OF FILE
