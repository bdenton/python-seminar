#!/usr/bin/python
#################################################################################
# FILENAME   : Parallel_Computing.py                                            #
# AUTHOR     : Brian Denton <brian.denton@gmail.com>                            #
# DATE       : 11/13/2012                                                       #
# DESCRIPTION: Demonstrate parallel computing with Python.                      #
#################################################################################

from datetime import datetime
from numpy.random import randint

# Greatest Common Divisor
def gcd(m,n):
    if n == 0:
        return m
    elif n > m:
        return gcd(n,m)
    else:
        return gcd(n,m%n)

# Range for tuples
LOW = 1
HIGH = 1E6
# Count of tuples
N = long(5E6)

# List of tuples
pairs = [ ( randint(LOW,HIGH),randint(LOW,HIGH) ) for i in range(N) ]


def gcd_sequential_search( data ):
    """
    Scan list of tuples and return pair with maximum GCD and value of that GCD.
    """
    max_gcd = -1
    max_gcd_pair = -1

    for i in range(len( data )):
        next_gcd = gcd( data[i][0], data[i][1] )
        if next_gcd > max_gcd:
            max_gcd = next_gcd
            max_gcd_pair = data[i]

    return max_gcd_pair, max_gcd

sequentialStart = datetime.now()

sequential = gcd_sequential_search( pairs )

sequentialStop = datetime.now()


def partition( lst, n):
    """ 
    Function to partition a list into n parts.
    """
    q, r = divmod( len(lst), n)
    indices = [q*i + min(i, r) for i in xrange(n+1)]
    return [lst[indices[i]:indices[i+1]] for i in xrange(n)]


def gcd_parallel_search( data, n_cpu ):
    """
    Split data into several parts, search each part for the tuple with maximal
    GCD. Then scan the results from each partition and find global result.
    """
    import pp
    partitions = partition( data, n_cpu )
    
    job_server = pp.Server( ncpus = n_cpu, ppservers = () )

    SubmittedJobs = []
    Result = []

    for part in partitions:
        f = job_server.submit( func = gcd_sequential_search,
                               args = (part,),
                               depfuncs = (gcd,) )
        SubmittedJobs.append( f )

    max_gcd = -1
    max_gcd_pair = -1
    for job in SubmittedJobs:
        job_result = job()
        if job_result[1] > max_gcd:
            max_gcd = job_result[1]
            max_gcd_pair = job_result[0]

    job_server.destroy()

    return max_gcd_pair, max_gcd

parallelStart = datetime.now()

parallel = gcd_parallel_search( pairs, 10 )

parallelStop = datetime.now()

sequentialElapsed = sequentialStop - sequentialStart
parallelElapsed = parallelStop - parallelStart

print "Sequential:", sequential, sequentialElapsed
print "Parallel:", parallel, parallelElapsed

## END OF FILE
