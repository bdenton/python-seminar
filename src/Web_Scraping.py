#!/usr/bin/python
#################################################################################
# FILENAME   : Web_Scraping.py                                                  #
# AUTHOR     : Brian Denton <brian.denton@gmail.com>                            #
# DATE       : 10/15/2012                                                       #
# DESCRIPTION: Scrape an RSS feed and print selected elements.                  #
#################################################################################

from urllib import urlopen
from BeautifulSoup import BeautifulStoneSoup

# Copy all content from the provided web page
LLY_RSS = urlopen( "http://apps.shareholder.com/rss/rss.aspx?channels=2886&companyid=LLY" ).read()

# Use BeautifulStoneSoup to parse webpage elements using XML tags
soup = BeautifulStoneSoup(LLY_RSS)

# Read the contents of each of the XML tags into a Python list
title = soup.findAll('title')
link = soup.findAll('link')
pubDate = soup.findAll('pubDate')
description = soup.findAll('description')

# Print title and link for each news story on Lilly RSS feed
for i in range(10):
    print title[i], "\n", link[i], "\n"


# Remove XML tags for prettier printing
for i in range(10):
    print "Title:", str(title[i]).strip('<title>').strip('</title>')
    print "Link: ", str(link[i]).strip('<link>').strip('</link>')
    print "\n"


## END OF FILE
